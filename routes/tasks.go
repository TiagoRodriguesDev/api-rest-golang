package routes

import (
	"api_rest/database"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
)

type Task struct {
	ID          int    `json:"id"`
	Description string `json:"Descrição"`
}

var tasks []Task

func GetTasks(w http.ResponseWriter, r *http.Request) {

	conn := database.SetConnection()
	defer conn.Close()

	selDB, err := conn.Query("select *from task")

	if err != nil {
		fmt.Println("Error to fetch", err)
	}

	for selDB.Next() {
		var task Task

		err = selDB.Scan(&task.ID, &task.Description)
		tasks = append(tasks, task)
	}

	encoder := json.NewEncoder(w)
	encoder.Encode(tasks)
}

func NewTask(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusCreated)
	conn := database.SetConnection()
	defer conn.Close()

	var register Task

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprint(w, "Bad Request")
	}

	json.Unmarshal(body, &register)

	action, err := conn.Prepare("insert into task (descricao) values(?)")
	action.Exec(register.Description)

	encoder := json.NewEncoder(w)
	encoder.Encode(register)
}

func GetTaskByID(w http.ResponseWriter, r *http.Request) {
	conn := database.SetConnection()
	defer conn.Close()
	var task Task

	vars := mux.Vars(r)
	id := vars["taskID"]

	selDB := conn.QueryRow("select *from task where id=" + id)

	selDB.Scan(&task.ID, &task.Description)

	encoder := json.NewEncoder(w)
	encoder.Encode(task)
}

func UpdateTask(w http.ResponseWriter, r *http.Request) {
	var register Task

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprint(w, "Bad Request")
	}
	json.Unmarshal(body, &register)

	var findIndex = -1

	for chave, valor := range tasks {
		if valor.ID == register.ID {
			findIndex = chave
		}
	}

	if findIndex > 0 {
		tasks[findIndex] = register
		encoder := json.NewEncoder(w)
		encoder.Encode(register)
	}
}
