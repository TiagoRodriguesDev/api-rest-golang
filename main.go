package main

import (
	"fmt"
	"net/http"

	"api_rest/routes"

	"github.com/gorilla/mux"
)

func majorRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Bem Vindo !")
}

func setRoutes(router *mux.Router) {
	router.HandleFunc("/", majorRoute)
	router.HandleFunc("/tasks", routes.GetTasks)
	router.HandleFunc("/tasks/{taskID}", routes.GetTaskByID)
	router.HandleFunc("/newtask", routes.NewTask)
	router.HandleFunc("/updatetask", routes.UpdateTask)
}

func main() {
	var router *mux.Router

	router = mux.NewRouter()

	setRoutes(router)

	err := http.ListenAndServe(":1617", router)
	if err != nil {
		fmt.Println("Error", err)
	}
}
