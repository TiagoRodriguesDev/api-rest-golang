package database

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

func SetConnection() *sql.DB {

	db, err := sql.Open("mysql", "root:root@/api_golang")
	if err != nil {
		panic(err)
	}

	return db

}
